const mongoose = require('mongoose')

const subjectSchema = new mongoose.Schema({
    title: String,
    teacher: String,
    grades: [],
    activityPluses: []
})

const Subject = mongoose.model('Subject', subjectSchema)

module.exports = Subject
