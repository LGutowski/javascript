const mongoose = require('mongoose')

const classSchema = new mongoose.Schema({
    name: String,
    educator: String,
    studentsAmmount: Number,
    subjects: []
})

const Class = mongoose.model('Class', classSchema)

module.exports = Class
