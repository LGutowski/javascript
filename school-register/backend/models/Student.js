const mongoose = require('mongoose')

const studenSchema = new mongoose.Schema({
    name: String,
    lastName: String,
    birthDate: Date,
    subjects: []
})

const Student = mongoose.model('Student', studenSchema)

module.exports = Student
