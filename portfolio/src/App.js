import React, { Component } from 'react';
import { Route } from 'react-router-dom';

import Layout from './hoc/Layout/Layout';
import NavBar from './components/Navigation/NavBar/NavBar';
import HomePage from './containers/HomePage/HomePage';
import AboutMe from './containers/AboutMe/AboutMe';
import Footer from './components/Footer/Footer';
import Technologies from './containers/Technologies/Technologies';


class App extends Component {
  state = {
    effectPercentage: 100
  };

  componentDidMount() {
    document.addEventListener('scroll', () => {
        window.scrollY/3 <= 100
        ? this.setState({effectPercentage: 100 - window.scrollY/3})
        : this.setState({effectPercentage: 0})  
    })
}

  render() {
    return (
      <Layout>
        <NavBar effectPercentage={this.state.effectPercentage} />
        <Route exact path='/' render={() => <HomePage effectPercentage={this.state.effectPercentage} />}/> 
        <Route exact path='/' render={() => <AboutMe effectPercentage={this.state.effectPercentage} />}/>
        <Route path="/technologies" render={() => <Technologies />} />
        <Footer effectPercentage={this.state.effectPercentage}/>
      </Layout>
    );
  }
}

export default App;
