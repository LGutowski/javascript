import React from 'react';
import { withRouter } from 'react-router-dom';

import classes from './Footer.css';

const footer = ({ effectPercentage, history }) => {

    const footerClasses = [classes.Footer]

    if (history.location.pathname !== '/') {
        footerClasses.push(classes.Background)
    } else if (effectPercentage < 30) {
        footerClasses.push(classes.Background)
    }
    
    return (
        <div className={footerClasses.join(' ')}>
            <div>Łukasz Gutowski</div>
            <div>© 2019 All Rights Reserved</div>
        </div>
    );
}

export default withRouter(footer);