import React from 'react';
import { withRouter } from "react-router";

import classes from './NavBar.css';

import NavigationItems from '../NavigationItems/NavigationItems';

const navBar = ({ effectPercentage, history }) => {

    const navigateToHomePage = () => {
        history.push('/') 
    }   

    const navBarClasses = [classes.NavBar]

    if (effectPercentage < 30) {
        navBarClasses.push(classes.Background)
    }

        return (         
            <>
                <div className={navBarClasses.join(' ')}>
                    <div className={classes.Title}>
                        <h1 onClick={navigateToHomePage}>LGutowski</h1>
                    </div>
                    <div className={classes.Navigation}>
                        <div className={classes.NavigationWrapper}>
                            <NavigationItems />
                        </div>
                    </div>
                </div>
                <div className={classes.Border} style={{width: effectPercentage+'%', opacity: (effectPercentage)/100}}></div>
            </>
        )
}

export default withRouter(navBar);