import React from 'react';
import { FaLinkedinIn, FaFacebookF } from 'react-icons/fa';
import classes from './NavigationItems.css';

import NavigationItem from '../NavigationItem/NavigationItem';

const navigationItems = () => {
    return (
        <ul className={classes.NavigationItems}>
            <NavigationItem link='/technologies'>Technologies</NavigationItem>
            <NavigationItem link='/portfolio'>Portfolio</NavigationItem>
            <NavigationItem link='/contact'>Contact</NavigationItem>
            <NavigationItem link='#'><FaFacebookF style={{fontSize:'1.5em'}} /></NavigationItem>
            <NavigationItem link='#'><FaLinkedinIn style={{fontSize:'1.5em'}} /></NavigationItem>
        </ul>
    );
}

export default navigationItems;