import React from 'react';

import classes from './Technologies.css';

const technologies = (props) => {

    return (
        <div className={classes.Technologies}>
            <i className="devicon-css3-plain"></i>
            <i className="devicon-sass-original"></i>
            <i className="devicon-html5-plain"></i>
            <i className="devicon-javascript-plain"></i>
            <i className="devicon-react-original"></i>
            <i className="devicon-nodejs-plain-wordmark"></i>
            <i className="devicon-webpack-plain"></i>
        </div>
    );
}

export default technologies;