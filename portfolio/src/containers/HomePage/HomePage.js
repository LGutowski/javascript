import React from 'react';

import classes from './HomePage.css';

const homePage = ({ effectPercentage }) => {

    const mainSectionClasses = [classes.MainSection]
    const arrowContainerClasses = [classes.ArrowContainer]
    const arrowDescrptionClasses = [classes.ArrowDescrption]

    if (effectPercentage === 0) {
        mainSectionClasses.push(classes.OpacityAnimation);
        arrowContainerClasses.push(classes.OpacityAnimation);
        arrowDescrptionClasses.push(classes.OpacityAnimation)
    }
    
    return (
            <div className={classes.HomePage}>
                <div className={mainSectionClasses.join(' ')}>
                    <h1><span>Hello! I'm <span className={classes.Bold}>Łukasz Gutowski.</span></span><br />I'm javascript Front End Developer.</h1>
                </div>
                <div className={arrowContainerClasses.join(' ')}>
                    <div className={classes.Arrow} style={{height: effectPercentage - 40 +'vh', opacity: (effectPercentage)/100}}>
                            <div className={classes.ArrowLeft}></div>
                            <div className={classes.ArrowRight}></div>
                    </div>
                    <h1 className={arrowDescrptionClasses} style={{opacity: (effectPercentage)/100}}>Scroll down to see more</h1>           
                </div>
            </div>
        );
}

export default homePage;