import React from 'react';

import classes from './AboutMe.css';

import imgUrl from '../../assets/images/about_image.JPG';

const aboutMe = ({ effectPercentage }) => {

    const imageClasses = [classes.AboutImage]
    const textClasses = [classes.AboutText]
    const backgroundBoxClasses = [classes.BackgroundBox]

    if (effectPercentage === 0) {
        imageClasses.push(classes.Background);
        textClasses.push(classes.Movement)
        backgroundBoxClasses.push(classes.Background)
    }

    return(
            <div className={classes.AboutMe}>
                <span className={textClasses.join(' ')}>
                    <span>&emsp;Hello, it is me! I'm the Front-End Developer working in Warsaw who has found his passion in designing applications using ReactJS. Outside of work, I am a maniac of a healthy lifestyle who feels best at the gym with rock music in my ear pods! Feel free to dive deeper into my portfolio and let me know if You like it!</span>
                    <span>Click <span className={classes.Link}>here</span> to contact me</span>                
                </span>
                <img className={imageClasses.join(' ')} src={imgUrl} alt="img" />
                <span className={backgroundBoxClasses.join(' ')}></span>
            </div>
    );
}

export default aboutMe;