const config = require('./config/config')
const app = require('./config/express')
const bodyParser = require('body-parser')
const mongoose = require('mongoose')

const db = mongoose.connect(config.mongo.uri, {
    useNewUrlParser: true
})

mongoose.connection.once('open', () => {
    console.log(`Connected to database`)
})

mongoose.connection.on('error', () => {
    throw new Error(`Unable to connect to database`)
})

app.listen(config.port, () => {
    console.log(`Server listening on port ${config.port}`)
})

module.exports = app