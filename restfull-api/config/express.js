const express = require('express')
const config = require('../config/config')
const routes = require('../api/routes/index')
const bodyParser = require('body-parser')
const helmet = require('helmet')
const cors = require('cors')
const morgan = require('morgan')
const compression = require('compression')
const expressWinston = require('express-winston')
const { http: logger } = require('./logger')

const app = express()

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

app.use(helmet())
app.use(cors())
app.use(compression())

if (config.env === 'development') {
    app.use(morgan('dev'))
    expressWinston.requestWhitelist.push('body')
    expressWinston.responseWhitelist.push('body')
    app.use(expressWinston.logger({
        winstonInstance: logger,
        meta: true,
        msg: 'HTTP {{req.method}} {{req.url}} {{res.statusCode}} {{res.responseTime}}ms',
        colorStatus: true
    }))
}

if (config.env === 'production') {
    app.use(morgan('common'))
    expressWinston.requestWhitelist.push('body')
    expressWinston.responseWhitelist.push('body')
    app.use(expressWinston.logger({
        winstonInstance: logger,
        msg: 'HTTP {{req.method}} {{req.url}} {{req.body}} {{res.statusCode}} {{res.responseTime}}ms',
        colorStatus: true
    }))
}



app.use('/', routes)

module.exports = app