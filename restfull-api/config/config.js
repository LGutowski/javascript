require('dotenv').config()

const config = {
    env: process.env.NODE_ENV,
    port: process.env.PORT,
    mongo: {
        uri: 'mongodb://127.0.0.1:27017/movieAPI'
    }
}

module.exports = config