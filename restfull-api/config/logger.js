const { createLogger, transports, format } = require('winston')
const config = require('./config')

const console = createLogger({

    transports: [

        new transports.Console({
            level: 'silly',
            silent: config.env === 'test',
            format: format.combine(
                format.colorize(),
                format.simple()
            )
        }),

        new transports.File({
            level: 'info',
            silent: config.env !== 'production',
            tailable: true,
            maxsize: 1048576,
            maxFiles: 1,
            format: format.combine(
                format.timestamp(),
                format.json()
            ),
            filename: './logs/logs.log'
        })

    ]

})

const http = createLogger({

    transports: [

        new transports.Console({
            level: 'silly',
            silent: config.env !== 'development',
            format: format.printf(info => JSON.stringify(info, null, 4))
        }),

        new transports.File({
            level: 'info',
            silent: config.env !== 'production',
            tailable: true,
            maxsize: 1048576,
            maxFiles: 1,
            format: format.combine(
                format.timestamp(),
                format.json()
            ),
            filename: './logs/logs.log'
        })

    ]

})

module.exports = { console, http }
