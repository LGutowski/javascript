**MovieAPI**
----
  _A movie database interacting with external API_

  
* **Notes:**

  `npm install`

  To run:

  `npm start`

* **URL**

  _/movies and /comments_

* **Method:**
  
  `GET` | `POST`
  
*  **URL Params**

   `GET`

    _/movies or /comments_

   _Fetch list of all movies or comments already present in application database_


    `POST` 
    
    _/comments_

    **Required:**

   `content=[string]`

   example: content=comment

    _/movies_

   **Optional:**
 
   `s=[string]`

   example: s=Star Wars

   `type=[string]`
   
   example: type=movie/series/episode

   `y=[string]`

   example: y=2018

* **Data Params**

  **Example:**

  {
      "s": "Star Wars",
      "y": "2017"
  }

    **Example:**

  {
      "content": "Comment nr 1"
  }

* **Success Response:**
  
  * **Code:** 200 <br />
    **Content:** `{
        "_id": "5beb0922203657828e17a7a7",
        "content": "Comment nr 1",
        "createdAt": "2018-11-13T17:25:54.190Z"
    }`

  * **Code:** 200 <br />
    **Content:** `{
        "_id": "5beb0999203657828e17a7a9",
        "poster": "https://m.media-amazon.com/images/M/MV5BNzVlY2MwMjktM2E4OS00Y2Y3LWE3ZjctYzhkZGM3YzA1ZWM2XkEyXkFqcGdeQXVyNzkwMjQ5NzM@._V1_SX300.jpg",
        "type": "movie",
        "year": "1977",
        "title": "Star Wars: Episode IV - A New Hope",
        "__v": 0
    }`
 