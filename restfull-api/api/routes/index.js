const express = require('express')
const moviesController = require('../controllers/movies.controller')
const commentsController = require('../controllers/comments.controller')

const router = express.Router()

router.route('/')
    .get((req, res) => {
        res.send('Welcome to Movie API')
    })

router.route('/movies')
    .get(moviesController.getMovies)
    .post(moviesController.saveMovie)

router.route('/comments')
    .get(commentsController.getComments)
    .post(commentsController.saveComment)

module.exports = router