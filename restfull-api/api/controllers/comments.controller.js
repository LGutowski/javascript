const Comment = require('../models/CommentModel')

function getComments(req, res, next) {
    Comment.get(req.params)
        .then(model => res.json(model))
        .catch(err => next(err))
}

function saveComment(req, res) {
    comment = req.body
    let newComment = new Comment(comment)
    newComment.save(function (err) {
        if (err) res.send(err)
        else res.send(newComment)
    })
}

module.exports = {
    getComments,
    saveComment
}