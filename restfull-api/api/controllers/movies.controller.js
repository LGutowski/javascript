const { get } = require('request-promise')
const Movie = require('../models/MovieModel')

function getMovies(req, res, next) {
    Movie.get(req.params)
        .then(model => res.json(model))
        .catch(err => next(err))
}

function saveMovie(req, res, next) {

    get({ uri: 'http://www.omdbapi.com/?apikey=40eaa253', json: true, qs: req.body })
        .then(resp => resp.Error ? Promise.reject(new Error(resp.Error)) : resp.Search)
        .then(movies => movies.map(movie =>
            Object.keys(movie).reduce((prev, curr) => ({ [curr.toLowerCase()]: movie[curr], ...prev }), {})))
        .then(movies => Promise.all(movies.map(movie => Movie.generate(movie))))
        .then(resp => res.json(resp))
        .catch(err => next(err))

}

module.exports = {
    getMovies,
    saveMovie
}