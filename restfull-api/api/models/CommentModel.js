const mongoose = require('mongoose')
const Schema = mongoose.Schema

const CommentModel = new Schema({
    content: { type: String, required: true },
    createdAt: Date
}, {
        versionKey: false
    })

CommentModel.pre('save', function () {
    if (!this.createdAt) {
        this.createdAt = new Date
    }
})

CommentModel.statics = {
    get(query = {}) {
        return this.find(query)
            .then(model => {
                if (!model)
                    Promise.reject(new Error('No comments in database yet'))
                return model
            })
    }
}

module.exports = mongoose.model('Comment', CommentModel)