const mongoose = require('mongoose')
const Schema = mongoose.Schema

const MovieModel = new Schema({
    title: String,
    year: String,
    imdbID: String,
    type: String,
    poster: String
})

MovieModel.statics = {
    get(query = {}) {
        return this.find(query)
            .then(model => {
                if (!model)
                    return Promise.reject(new Error('No movie in db'))
                return model
            })
    },
    generate(document) {
        return this.findOne({title: document.title})
            .then(model => {
                if (!model)
                    return this.create(document)
                return model
            })
    }
}

module.exports = mongoose.model('Movie', MovieModel)